#!/bin/bash

while :
do
((count = 0))
while read line || [ -n "$line" ]; do
   [[ "$line" = "\#*" ]] && continue
   ((count++))
   if [ "$(curl -s --head  --request GET "$line" | grep "200")" ]; then
       mv "$count".new "$count".old -f
       curl "$line" -L --compressed -s > new.html
       mv new.html "$count".new -f
       DIFF_OUTPUT="$(diff $count.new $count.old)"
       if [ "0" != "${#DIFF_OUTPUT}" ]; then
               echo "$count $line modified"
               while :
                do
                  echo -ne '\007'
                  sleep 2
                done
       else
               echo "$count $line unmodified"
       fi
   else
       echo "$count $line FAILED" >&2
   fi
done <"$1"
sleep 5
done
